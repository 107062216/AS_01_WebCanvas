var canvas = document.getElementById('Canvas');
var ctx = canvas.getContext('2d');

var canvasEl = document.getElementById('colorCanvas');
var canvasContext = canvasEl.getContext('2d');

var x, y;
var startX, startY;

var cPushArray = new Array();
var lastImgArray = new Array();
var cStep = -1;

var slider = document.getElementById("Range");
ctx.lineWidth = slider.value;
ctx.font = "12px Arial";
var font_px = '12';
var area_px = '14';
var font_type = ' Arial';

var palette;
var colorstyle = "#000000";

var textarea = null;

ctx.lineJoin = ctx.lineCap = 'round';

var imageLoader = document.getElementById('upload_img');
imageLoader.addEventListener('change', handleImage, false);

initColorPicker();

function changeFontPx(val0, val1) {
    clearTextArea();
    ctx.font = val0 + 'px' + font_type;
    font_px = val0;
    area_px = val1;
    var font_val = document.getElementById("dropdownMenuButton_px");
    font_val.innerHTML = val0;
}

function changeFontType(val) {
    clearTextArea();
    ctx.font = font_px + 'px' + val;
    font_type = val;
    var font_val = document.getElementById("dropdownMenuButton_type");
    font_val.innerHTML = val;
}

function changeBrush(val) {
    clearTextArea();
    var brush_val = document.getElementById("dropdownMenuButton_brush");
    var button = document.getElementById("brush_button");
    var brush = document.getElementById("brush");
    switch (val) {
        case 0:
            paint(); changecursor_pen()
            brush.onclick = function(){paint(); changecursor_pen()};
            button.className = "fas fa-pencil-alt";
            brush_val.innerHTML = "Pencil";
            break;
        case 1:
            mikepen(); changecursor_pen()
            brush.onclick = function(){mikepen(); changecursor_pen()};
            button.className = "fas fa-marker";
            brush_val.innerHTML = "Marker";
            break;
        case 2:
            pen(); changecursor_pen()
            brush.onclick = function(){pen(); changecursor_pen()};
            button.className = "fas fa-pen-fancy";
            brush_val.innerHTML = "Pen";
            break;
    }
}


slider.oninput = function () {
    ctx.lineWidth = this.value;
}

function paint() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_paint);
    canvas.addEventListener('mouseup', mouseUp_paint, false);
}

function mouseDown_paint(event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    canvas.addEventListener('mousemove', mouseMove_paint, false);
}

function mouseUp_paint() {
    dPush();
    cPush();
    canvas.removeEventListener('mousemove', mouseMove_paint, false);
}

function mouseMove_paint(event) {
    dUndo();
    var mousePos = getMousePos(canvas, event);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    dPush();
};

function mikepen() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_mikepen);
    canvas.addEventListener('mouseup', mouseUp_mikepen, false);
}

function mouseDown_mikepen(event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    startX = mousePos.x, startY = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove_mikepen, false);
};

function mouseUp_mikepen() {
    dPush();
    cPush();
    ctx.globalAlpha = 1;
    canvas.removeEventListener('mousemove', mouseMove_mikepen, false);
};

function mouseMove_mikepen(event) {
    var mousePos = getMousePos(canvas, event);
    ctx.beginPath();

    ctx.globalAlpha = 1;
    ctx.moveTo(startX - 4, startY - 4);
    ctx.lineTo(mousePos.x - 4, mousePos.y - 4);
    ctx.stroke();

    ctx.globalAlpha = 0.6;
    ctx.moveTo(startX - 2, startY - 2);
    ctx.lineTo(mousePos.x - 2, mousePos.y - 2);
    ctx.stroke();

    ctx.globalAlpha = 0.4;
    ctx.moveTo(startX, startY);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();

    ctx.globalAlpha = 0.3;
    ctx.moveTo(startX + 2, startY + 2);
    ctx.lineTo(mousePos.x + 2, mousePos.y + 2);
    ctx.stroke();

    ctx.globalAlpha = 0.2;
    ctx.moveTo(startX + 4, startY + 4);
    ctx.lineTo(mousePos.x + 4, mousePos.y + 4);
    ctx.stroke();

    startX = mousePos.x, startY = mousePos.y;
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function pen() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_pen);
    canvas.addEventListener('mouseup', mouseUp_pen, false);
};

function mouseDown_pen (event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    startX = mousePos.x, startY = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove_pen, false);
};

function mouseUp_pen() {
    dPush();
    cPush();
    canvas.removeEventListener('mousemove', mouseMove_pen, false);
};

function mouseMove_pen (event) {
    var mousePos = getMousePos(canvas, event);
    ctx.beginPath();

    ctx.moveTo(startX - getRandomInt(0, 2), startY - getRandomInt(0, 2));
    ctx.lineTo(mousePos.x - getRandomInt(0, 2), mousePos.y - getRandomInt(0, 2));
    ctx.stroke();

    ctx.moveTo(startX, startY);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();

    ctx.moveTo(startX + getRandomInt(0, 2), startY + getRandomInt(0, 2));
    ctx.lineTo(mousePos.x + getRandomInt(0, 2), mousePos.y + getRandomInt(0, 2));
    ctx.stroke();

    startX = mousePos.x, startY = mousePos.y;
};

function eraser() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_eraser);
    canvas.addEventListener('mouseup', mouseUp_eraser, false);
}

function mouseDown_eraser(event) {
    init_draw();
    strokeStyle = "#FFFFFF";
    ctx.globalCompositeOperation = "destination-out";
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    canvas.addEventListener('mousemove', mouseMove_eraser, false);
}

function mouseUp_eraser() {
    dPush();
    cPush();
    ctx.globalCompositeOperation = "source-over";
    canvas.removeEventListener('mousemove', mouseMove_eraser, false);
}

function mouseMove_eraser(event) {
    dUndo();
    var mousePos = getMousePos(canvas, event);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    dPush();
};

function circle() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_circle);
    canvas.addEventListener('mouseup', mouseUp_circle, false);
}

function mouseDown_circle(event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    startX = mousePos.x, startY = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove_circle, false);
}

function mouseUp_circle() {
    dPush();
    cPush();
    canvas.removeEventListener('mousemove', mouseMove_circle, false);
}

function mouseMove_circle(event) {
    dUndo();
    var mousePos = getMousePos(canvas, event);
    ctx.beginPath();
    var Radius = Math.sqrt(Math.pow(startX - mousePos.x, 2) + Math.pow(startY - mousePos.y, 2)) / 2;
    ctx.arc((startX + mousePos.x) / 2, (startY + mousePos.y) / 2, Radius, 0, 2 * Math.PI);
    ctx.stroke();
    dPush();
};

function rect() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_rect);
    canvas.addEventListener('mouseup', mouseUp_rect, false);
}

function mouseDown_rect(event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    startX = mousePos.x, startY = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove_rect, false);
}

function mouseUp_rect() {
    dPush();
    cPush();
    canvas.removeEventListener('mousemove', mouseMove_rect, false);
}

function mouseMove_rect(event) {
    dUndo();
    var mousePos = getMousePos(canvas, event);
    ctx.strokeRect(startX, startY, mousePos.x - startX, mousePos.y - startY);
    dPush();
};

function tri() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_tri);
    canvas.addEventListener('mouseup', mouseUp_tri, false);
}

function mouseDown_tri(event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    startX = mousePos.x, startY = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove_tri, false);
}

function mouseUp_tri() {
    dPush();
    cPush();
    canvas.removeEventListener('mousemove', mouseMove_tri, false);
}

function mouseMove_tri(event) {
    dUndo();
    var mousePos = getMousePos(canvas, event);
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.lineTo(startX * 2 - mousePos.x, mousePos.y);
    ctx.closePath();
    ctx.stroke();
    dPush();
};

function line() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_line);
    canvas.addEventListener('mouseup', mouseUp_line, false);
}

function mouseDown_line(event) {
    init_draw();
    var mousePos = getMousePos(canvas, event);
    event.preventDefault();
    startX = mousePos.x, startY = mousePos.y;
    canvas.addEventListener('mousemove', mouseMove_line, false);
}

function mouseUp_line() {
    dPush();
    cPush();
    canvas.removeEventListener('mousemove', mouseMove_line, false);
}

function mouseMove_line(event) {
    dUndo();
    var mousePos = getMousePos(canvas, event);
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    dPush();
};

function text() {
    rmAllListener();
    canvas.addEventListener('mousedown', mouseDown_text);
}

function mouseDown_text(event) {
    init_draw();
    event.preventDefault();
    var mousePos = getMousePos(canvas, event);
    startX = mousePos.x, startY = mousePos.y;

    canvas.addEventListener('click', textonCanvas, false);
    document.addEventListener('keydown', inputText);
}

function textonCanvas(event) {
    if (!textarea) {
        textarea = document.createElement('textarea');
        textarea.className = 'info';
        document.body.appendChild(textarea);
    }
    textarea.style.display = "block";
    textarea.value = '';
    textarea.style.left = event.clientX + 'px';
    textarea.style.top = event.clientY + 'px';
    textarea.style.fontSize = font_px + 'px';
    textarea.style.height = area_px + 'px';
    textarea.style.fontFamily = font_type;
    textarea.style.color = colorstyle;
}

function inputText(event) {
    if (event.keyCode == 13) {
        textarea.style.display = "none";
        ctx.fillText(textarea.value, startX, startY);
        dPush();
        cPush();
    }
}

function clearTextArea() {
    if (textarea) {
        textarea.style.display = "none";
    }
}

function reset() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    cPushArray.splice(0, cPushArray.length);
    cStep = -1;
}

function getMousePos(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
};

function init_draw() {
    ctx.strokeStyle = colorstyle;
    ctx.fillStyle = colorstyle;
    lastImgArray.splice(0, lastImgArray.length);
    var lastImg = ctx.getImageData(0, 0, canvas.width, canvas.height);
    lastImgArray.push(lastImg);
}

function dPush() {
    var lastImg = ctx.getImageData(0, 0, canvas.width, canvas.height);
    lastImgArray[1] = lastImg;
}

function dUndo() {
    ctx.putImageData(lastImgArray[0], 0, 0);
}

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) {
        cPushArray.length = cStep;
    }
    cPushArray.push(lastImgArray[1]);
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        ctx.putImageData(cPushArray[cStep], 0, 0);
    }
    else {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        cStep = -1;
    }
}

function cRedo() {
    if (cStep < cPushArray.length - 1) {
        cStep++;
        ctx.putImageData(cPushArray[cStep], 0, 0);
    }
}

function rmAllListener() {
    canvas.removeEventListener('mousedown', mouseDown_paint);
    canvas.removeEventListener('mouseup', mouseUp_paint, false);
    canvas.removeEventListener('mousemove', mouseMove_paint, false);
    canvas.removeEventListener('mousedown', mouseDown_mikepen);
    canvas.removeEventListener('mouseup', mouseUp_mikepen, false);
    canvas.removeEventListener('mousemove', mouseMove_mikepen, false);
    canvas.removeEventListener('mousedown', mouseDown_pen);
    canvas.removeEventListener('mouseup', mouseUp_pen, false);
    canvas.removeEventListener('mousemove', mouseMove_pen, false);
    canvas.removeEventListener('mousedown', mouseDown_eraser);
    canvas.removeEventListener('mouseup', mouseUp_eraser, false);
    canvas.removeEventListener('mousemove', mouseMove_eraser, false);
    canvas.removeEventListener('mousedown', mouseDown_circle);
    canvas.removeEventListener('mouseup', mouseUp_circle, false);
    canvas.removeEventListener('mousemove', mouseMove_circle, false);
    canvas.removeEventListener('mousedown', mouseDown_rect);
    canvas.removeEventListener('mouseup', mouseUp_rect, false);
    canvas.removeEventListener('mousedown', mouseDown_tri);
    canvas.removeEventListener('mouseup', mouseUp_tri, false);
    canvas.removeEventListener('mousemove', mouseMove_tri, false);
    canvas.removeEventListener('mousedown', mouseDown_text);
    canvas.removeEventListener('mousedown', mouseDown_line);
    canvas.removeEventListener('mouseup', mouseUp_line, false);
    canvas.removeEventListener('mousemove', mouseMove_line, false);
    canvas.removeEventListener('click', textonCanvas, false);
    document.removeEventListener('keydown', inputText);
    clearTextArea();
}

function initColorPicker() {
    palette = new Image(256, 256);
    palette.onload = () => canvasContext.drawImage(palette, 0, 0, palette.width, palette.height);
    palette.crossOrigin = "anonymous";
    palette.src = "./src/palette/rgb.png";
}

function mouseDown_colorpick(event) {
    canvasContext.drawImage(palette, 0, 0, palette.width, palette.height);
    var imgData = canvasContext.getImageData(event.offsetX, event.offsetY, 1, 1);
    var rgba = imgData.data;

    colorstyle = "rgba(" + rgba[0] + ", " + rgba[1] + ", " + rgba[2] + ", " + rgba[3] + ")";
    canvasContext.beginPath();
    canvasContext.arc(event.offsetX, event.offsetY, 3, 0, 2 * Math.PI);
    canvasContext.stroke();
}

function handleImage(event0) {
    var reader = new FileReader();
    reader.onload = function (event1) {
        var img = new Image();
        img.onload = function () {
            ctx.drawImage(img, 0, 0);
            dPush();
            cPush();
        }
        img.src = event1.target.result;
    }
    reader.readAsDataURL(event0.target.files[0]);
}

function changecursor_pen() {
    canvas.style.cursor = 'url("./src/cursor/pencil.png"), auto';
}

function changecursor_eraser() {
    canvas.style.cursor = 'url("./src/cursor/eraser.png"), auto';
}

function changecursor_circle() {
    canvas.style.cursor = 'url("./src/cursor/circle.png"), auto';
}

function changecursor_rect() {
    canvas.style.cursor = 'url("./src/cursor/rect.png"), auto';
}

function changecursor_tri() {
    canvas.style.cursor = 'url("./src/cursor/tri.png"), auto';
}

function changecursor_line() {
    canvas.style.cursor = 'url("./src/cursor/line.png"), auto';
}

function changecursor_text() {
    canvas.style.cursor = 'url("./src/cursor/text.png"), auto';
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
