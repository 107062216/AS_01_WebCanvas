# Software Studio 2020 Spring
## Assignment 01 Web Canvas - 107062216 莊立維



### How to use 

#### Basic control tools

- Brush and eraser
    在網頁右邊的工具欄中，分別有筆刷![](https://i.imgur.com/zAvEKXY.png)和橡皮擦![](https://i.imgur.com/9XTP2Cz.png)的圖示，按下去之後即可在畫布上使用。
    
- Color selector
    在網頁右邊的工具欄中，有一個調色盤可以選取想要的顏色，點下去之後會有一個白點註記所選取的顏色。
    
- Simple Menu (brush size)
    在網頁右邊的工具欄中，有一個滑桿能夠調整畫筆的大小，同時也能夠調整圖形外框的大小。預設為15px，最小可以改為1px，最大可以改為30px。
    

#### Text input
- User can type texts on canvas
    我這邊是用textarea來實作，在網頁右邊的工具欄中，有一個Text![](https://i.imgur.com/NVh4l3m.png)的圖示，按下去之後即可在畫布上使用，點下去會在畫布上所點的位置出現文字方塊，即可輸入文字，輸入完後按Enter變會顯示在畫布上。文字方塊中顯示的文字，會隨著大小和顏色的不同有所改變，而文字方塊則會隨著文字大小而改變高度和寬度。

- Font Menu
    在網頁右邊的工具欄中，有兩個下拉式選單可以調整文字的字型和大小，而下拉示選單會顯示當下的的字型及大小，預設為12px Arial。若要改變文字顏色，用上面的調色盤改變即可。  
    
    
#### Cursor icon
- The image should change according to the currently used tool
    只要按下網頁右邊的工具欄的畫筆、橡皮擦、Text，及圖形功能，將游標移到畫布上時，會變成各自的游標圖案，如以下所示：
![](https://i.imgur.com/eMxS7Vl.png) pen
![](https://i.imgur.com/eL7aK38.png) eraser
![](https://i.imgur.com/NVh4l3m.png) text
![](https://i.imgur.com/CWeGNW5.png) circle
![](https://i.imgur.com/GmXFd4A.png) triangle
![](https://i.imgur.com/Bov8r81.png) rectangle
![](https://i.imgur.com/9vww6M6.png) line

#### Refresh button
- Reset canvas
    在網頁右邊的工具欄中，有一個垃圾桶![](https://i.imgur.com/LXRWugz.png)的圖示，會重置整個畫布，即按Undo不會回到畫布原本的狀態，能然是空白的畫布。
    
#### Different brush shapes
- Circle, rectangle and triangle
    在網頁右邊的工具欄中，分別有圓形![](https://i.imgur.com/CWeGNW5.png)、長方形![](https://i.imgur.com/Bov8r81.png)和三角形![](https://i.imgur.com/GmXFd4A.png)的圖示，按下去之後即可在畫布上使用。

- Drag the cursor to make circles, rectangles, and triangles of different size 
    在畫布上拖曳可以改變圖形的大小與長寬。

#### Un/Re-do button
- 在網頁右邊的工具欄中，分別有Undo![](https://i.imgur.com/kzOL5KE.png)和Redo![](https://i.imgur.com/4K9WOtk.png)的圖示，按下去之後即可使畫布Undo和Redo。

#### Image tool
- User can upload image and paste it 
    在網頁右邊的工具欄中，有一個上傳![](https://i.imgur.com/lC55lSe.png)的圖示，按下去之後可以選擇所要上傳的圖片，上傳後會以畫布左上角為基準顯示出來，而圖片不會進行縮放。

#### Download
- Download current canvas as an image file
    在網頁右邊的工具欄中，有一個下載![](https://i.imgur.com/ieJZc8a.png)的圖示，按下去之後會將當下的畫布存成image.png下載下來。






### Others Widgets

- Line
    在網頁右邊的工具欄中，有一個斜線![](https://i.imgur.com/9vww6M6.png)的圖示，按下去之後即可在畫布上使用。

- Different Brushes 
    在網頁右邊的工具欄中，有一個下拉式選單可以改變筆刷風格，有鉛筆、麥克筆和鋼筆三種，預設為鉛筆。筆刷按鈕會隨著所選不同而改變，選完之後再點一次筆刷按鈕，就可以換成想要的筆刷風格。


### Gitlab page link

https://107062216.gitlab.io/AS_01_WebCanvas

